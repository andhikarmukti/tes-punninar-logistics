<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PowerUnit extends Model
{
    // protected $table = 'power_unit';
    protected $guarded = ['id_power_unit'];
    protected $primaryKey = 'id_power_unit';

    const CREATED_AT = 'insert_date';
    const UPDATED_AT = 'update_date';

    public function corporation()
    {
        return $this->hasOne(Corporation::class, 'id_corporation', 'id_corporation');
    }

    public function location()
    {
        return $this->hasOne(Location::class, 'id_location', 'id_location');
    }

    public function powerUnitType()
    {
        return $this->hasOne(PowerUnitType::class, 'id_power_unit_type', 'id_power_unit_type');
    }

    public function user_insert()
    {
        return $this->hasOne(User::class, 'id', 'insert_user');
    }

    public function user_udpate()
    {
        return $this->hasOne(User::class, 'id', 'udpate_user');
    }
}
