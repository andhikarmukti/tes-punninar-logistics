<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GnMhLookup extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'lookup_id';
    protected $guarded = ['lookup_id'];

    public function gnMdLookup()
    {
        return $this->hasMany(GnMdLookup::class, 'lookup_lines_id', 'lookup_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'insert_user');
    }
}
