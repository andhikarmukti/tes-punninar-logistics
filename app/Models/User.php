<?php

namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function powerUnits_insert()
    {
        return $this->hasMany(PowerUnit::class, 'insert_user', 'id');
    }

    public function corporations_insert()
    {
        return $this->hasMany(Corporation::class, 'insert_user', 'id');
    }

    public function locations_insert()
    {
        return $this->hasMany(Location::class, 'insert_user', 'id');
    }

    public function powerUnitTypes_insert()
    {
        return $this->hasMany(PowerUnitType::class, 'insert_user', 'id');
    }

    public function gnMhLookups_insert()
    {
        return $this->hasMany(GnMhLookup::class, 'insert_user', 'id');
    }

    public function gnMdLookups_insert()
    {
        return $this->hasMany(GnMdLookup::class, 'insert_user', 'id');
    }

    //

    public function powerUnits_update()
    {
        return $this->hasMany(PowerUnit::class, 'udpate_user', 'id');
    }

    public function corporations_update()
    {
        return $this->hasMany(Corporation::class, 'udpate_user', 'id');
    }

    public function locations_update()
    {
        return $this->hasMany(Location::class, 'udpate_user', 'id');
    }

    public function powerUnitTypes_update()
    {
        return $this->hasMany(PowerUnitType::class, 'udpate_user', 'id');
    }

    public function gnMhLookups_update()
    {
        return $this->hasMany(GnMhLookup::class, 'udpate_user', 'id');
    }

    public function gnMdLookups_update()
    {
        return $this->hasMany(GnMdLookup::class, 'udpate_user', 'id');
    }
}
