<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GnMdLookup extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'lookup_lines_id';
    protected $guarded = ['lookup_lines_id'];

    public function gnMhLookup()
    {
        return $this->hasOne(GnMhLookup::class, 'lookup_id', 'lookup_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'insert_user');
    }
}
