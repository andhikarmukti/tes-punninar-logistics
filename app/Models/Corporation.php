<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Corporation extends Model
{
    protected $guarded = ['id_corporation'];
    protected $primaryKey = 'id_corporation';

    const CREATED_AT = 'insert_date';
    const UPDATED_AT = 'update_date';

    public function powerUnits()
    {
        return $this->hasMany(PowerUnit::class, 'id_power_unit', 'id_corporation');
    }

    public function user_insert()
    {
        return $this->hasOne(User::class, 'id', 'insert_user');
    }

    public function user_update()
    {
        return $this->hasOne(User::class, 'id', 'update_user');
    }
}
