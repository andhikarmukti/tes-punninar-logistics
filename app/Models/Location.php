<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $guarded = ['id_location'];
    protected $primaryKey = 'id_location';

    const CREATED_AT = 'insert_date';
    const UPDATED_AT = 'update_date';

    public function powerUnits()
    {
        return $this->hasMany(PowerUnit::class, 'id_location', 'id_location');
    }

    public function user_insert()
    {
        return $this->hasOne(User::class, 'id', 'insert_user');
    }

    public function user_update()
    {
        return $this->hasOne(User::class, 'id', 'update_user');
    }
}
