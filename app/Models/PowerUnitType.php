<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PowerUnitType extends Model
{
    protected $guarded = ['id_power_unit_type'];
    protected $primaryKey = 'id_power_unit_type';

    const CREATED_AT = 'insert_date';
    const UPDATED_AT = 'update_date';

    public function powerUnits()
    {
        return $this->hasMany(PowerUnit::class, 'id_power_unit', 'id_power_unit_type');
    }

    public function user_insert()
    {
        return $this->hasOne(User::class, 'id', 'insert_user');
    }

    public function user_update()
    {
        return $this->hasOne(User::class, 'id', 'update_user');
    }
}
