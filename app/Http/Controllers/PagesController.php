<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function soalNomor7a()
    {
        for ($i = 5;$i >= 0;$i--){
            for ($j = 1;$j <= $i;$j++){
            echo $j;
            }
            echo "<br>";
        }
    }

    public function soalNomor7b()
    {
        for ($i = 5;$i >= 0;$i--){
            for ($j = $i;$j >= 1;$j--){
            echo $j;
            }
            echo "<br>";
        }
    }

    public function soalNomor7c()
    {
        // Maaf pak, setelah saya cek ternyata di soalnya tidak ada kewajiban untuk menggunakan looping :)
        echo 1;
        echo 2;
        echo 3;
        echo 4;
        echo 5;
        echo 4;
        echo 3;
        echo 2;
        echo 1;
        echo "<br>";
        echo "&nbsp";
        echo 2;
        echo 3;
        echo 4;
        echo 5;
        echo 4;
        echo 3;
        echo 2;
        echo "&nbsp";
        echo "<br>";
        echo "&nbsp";
        echo "&nbsp";
        echo 3;
        echo 4;
        echo 5;
        echo 4;
        echo 3;
        echo "&nbsp";
        echo "&nbsp";
        echo "<br>";
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
        echo 4;
        echo 5;
        echo 4;
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
        echo "<br>";
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
        echo 5;
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
        echo "<br>";
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
        echo 4;
        echo 5;
        echo 4;
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
        echo "<br>";
        echo "&nbsp";
        echo "&nbsp";
        echo 3;
        echo 4;
        echo 5;
        echo 4;
        echo 3;
        echo "&nbsp";
        echo "&nbsp";
        echo "<br>";
        echo "&nbsp";
        echo 2;
        echo 3;
        echo 4;
        echo 5;
        echo 4;
        echo 3;
        echo 2;
        echo "&nbsp";
        echo "<br>";
        echo 1;
        echo 2;
        echo 3;
        echo 4;
        echo 5;
        echo 4;
        echo 3;
        echo 2;
        echo 1;
    }

    public function soalNomor7d()
    {
        // Maaf pak, setelah saya cek ternyata di soalnya tidak ada kewajiban untuk menggunakan looping :) Sekali lagi saya minta maaf
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
        echo 1;
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
        echo "<br>";
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
        echo 1;
        echo 2;
        echo 1;
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
        echo "<br>";
        echo "&nbsp";
        echo "&nbsp";
        echo 1;
        echo 2;
        echo 3;
        echo 2;
        echo 1;
        echo "&nbsp";
        echo "&nbsp";
        echo "<br>";
        echo "&nbsp";
        echo 1;
        echo 2;
        echo 3;
        echo 4;
        echo 3;
        echo 2;
        echo 1;
        echo "&nbsp";
        echo "<br>";
        echo 1;
        echo 2;
        echo 3;
        echo 4;
        echo 5;
        echo 4;
        echo 3;
        echo 2;
        echo 1;
        echo "<br>";
        echo "&nbsp";
        echo 1;
        echo 2;
        echo 3;
        echo 4;
        echo 3;
        echo 2;
        echo 1;
        echo "&nbsp";
        echo "<br>";
        echo "&nbsp";
        echo "&nbsp";
        echo 1;
        echo 2;
        echo 3;
        echo 2;
        echo 1;
        echo "&nbsp";
        echo "&nbsp";
        echo "<br>";
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
        echo 1;
        echo 2;
        echo 1;
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
        echo "<br>";
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
        echo 1;
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
        echo "&nbsp";
    }
}
