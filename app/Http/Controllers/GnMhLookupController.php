<?php

namespace App\Http\Controllers;

use App\Models\GnMhLookup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GnMhLookupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Master Lookup GN MH';
        $gn_mhs = GnMhLookup::all();

        return view('master_lookup.gn_mh.index', compact(
            'title',
            'gn_mhs'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'lookup_code' => 'required',
            'description' => 'required'
        ]);

        GnMhLookup::create([
            'lookup_code' => $request->lookup_code,
            'description' => $request->description,
            'insert_user' => auth()->user()->id,
            'insert_time' => now()
        ]);

        return back()->with('success', 'Berhasil menambah data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GnMhLookup  $gnMhLookup
     * @return \Illuminate\Http\Response
     */
    public function show(GnMhLookup $gnMhLookup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GnMhLookup  $gnMhLookup
     * @return \Illuminate\Http\Response
     */
    public function edit(GnMhLookup $gnMhLookup, $id)
    {
        $title = 'Edit Gn Mh';
        $gn_mh = $gnMhLookup->find($id);

        return view('master_lookup.gn_mh.edit', compact(
            'title',
            'gn_mh'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GnMhLookup  $gnMhLookup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GnMhLookup $gnMhLookup, $id)
    {
        $request->validate([
            'lookup_code' => 'required',
            'description' => 'required'
        ]);

        $gnMhLookup->find($id)->update([
            'lookup_code' => $request->lookup_code,
            'description' => $request->description,
            'insert_user' => auth()->user()->id,
            'insert_time' => now()
        ]);

        return redirect('/master-lookup/gn-mh')->with('edit', 'Berhasil merubah data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GnMhLookup  $gnMhLookup
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        GnMhLookup::find($id)->delete();
        return response()->json([
            'success' => true
        ], 200);
    }
}
