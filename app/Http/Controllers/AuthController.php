<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function loginIndex()
    {
        $title = 'Login';
        return view('auth.login', compact(
            'title'
        ));
    }

    public function login(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if(!$user){
            return redirect()->route('login')->with('error','Email-Address And Password Are Wrong.');
        }

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password]))
        {
            return redirect()->route('power-unit.index');
        }else{
            return redirect()->route('login')->with('error','Email-Address And Password Are Wrong.');
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('/login');
    }
}
