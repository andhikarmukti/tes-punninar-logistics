<?php

namespace App\Http\Controllers;

use App\Models\GnMdLookup;
use App\Models\GnMhLookup;
use Illuminate\Http\Request;

class GnMdLookupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Master Lookup GN MD';
        $gn_mhs = GnMhLookup::all();
        $gn_mds = GnMdLookup::all();

        return view('master_lookup.gn_md.index', compact(
            'title',
            'gn_mhs',
            'gn_mds'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'lookup_id' => 'required',
            'lookup_lines_code' => 'required',
            'description' => 'required',
            'effective_from' => 'required',
            'effective_to' => 'required'
        ]);

        GnMdLookup::create([
            'lookup_id' => $request->lookup_id,
            'lookup_lines_code' => $request->lookup_lines_code,
            'description' => $request->description,
            'effective_from' => $request->effective_from,
            'effective_to' =>$request->effective_to,
            'insert_user' => auth()->user()->id,
            'insert_time' => now()
        ]);

        return back()->with('success', 'Berhasil menambah data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GnMdLookup  $gnMdLookup
     * @return \Illuminate\Http\Response
     */
    public function show(GnMdLookup $gnMdLookup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GnMdLookup  $gnMdLookup
     * @return \Illuminate\Http\Response
     */
    public function edit(GnMdLookup $gnMdLookup, $id)
    {
        $title = 'Edit Gn Md';
        $gn_md = $gnMdLookup->find($id);
        $gn_mhs = GnMhLookup::all();

        return view('master_lookup.gn_md.edit', compact(
            'title',
            'gn_md',
            'gn_mhs'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GnMdLookup  $gnMdLookup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GnMdLookup $gnMdLookup, $id)
    {
        $request->validate([
            'lookup_id' => 'required',
            'lookup_lines_code' => 'required',
            'description' => 'required',
            'effective_from' => 'required',
            'effective_to' => 'required'
        ]);

        $gnMdLookup->find($id)->update([
            'lookup_id' => $request->lookup_id,
            'lookup_lines_code' => $request->lookup_lines_code,
            'description' => $request->description,
            'effective_from' => $request->effective_from,
            'effective_to' =>$request->effective_to,
            'insert_user' => auth()->user()->id,
            'insert_time' => now()
        ]);

        return redirect('/master-lookup/gn-md')->with('update', 'Berhasil merubah data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GnMdLookup  $gnMdLookup
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        GnMdLookup::find($id)->delete();
        return response()->json([
            'success' => true
        ], 200);
    }
}
