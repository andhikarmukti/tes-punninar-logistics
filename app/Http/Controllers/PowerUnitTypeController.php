<?php

namespace App\Http\Controllers;

use App\Models\PowerUnitType;
use Illuminate\Http\Request;

class PowerUnitTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PowerUnitType  $powerUnitType
     * @return \Illuminate\Http\Response
     */
    public function show(PowerUnitType $powerUnitType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PowerUnitType  $powerUnitType
     * @return \Illuminate\Http\Response
     */
    public function edit(PowerUnitType $powerUnitType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PowerUnitType  $powerUnitType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PowerUnitType $powerUnitType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PowerUnitType  $powerUnitType
     * @return \Illuminate\Http\Response
     */
    public function destroy(PowerUnitType $powerUnitType)
    {
        //
    }
}
