<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\PowerUnit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Location';
        $locations = Location::all();

        return view('location.index', compact(
            'title',
            'locations'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location, $id)
    {
        $title = 'Detail Location';

        $detail_location = $location->find($id);
        $power_unit = new PowerUnit;
        $power_units_unique = PowerUnit::orderBy('power_unit_num', 'DESC')->distinct('power_unit_num')->get();
        $power_units_latest = [];
        foreach($power_units_unique as $power_unit_unique){
            $power_unit_latest = $power_unit->where('power_unit_num', $power_unit_unique->power_unit_num)->orderBy('update_date', 'DESC')->first();
            array_push($power_units_latest, $power_unit_latest);
        }

        $total_power_units = [];
        foreach($power_units_latest as $power_unit){
            if($power_unit->id_location == $detail_location->id_location){
                array_push($total_power_units, $power_unit);
            }
        }

        return view('location.detail', compact(
            'detail_location',
            'total_power_units',
            'title'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        //
    }
}
