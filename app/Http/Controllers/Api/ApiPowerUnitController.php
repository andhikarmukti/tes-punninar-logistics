<?php

namespace App\Http\Controllers\Api;

use App\Models\PowerUnit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ApiPowerUnitController extends Controller
{

    public function index()
    {
        $power_unit_nums = PowerUnit::where('is_active', 'Y')->pluck('power_unit_num')->unique();
        $power_unit = new PowerUnit;
        $power_units = [];

        foreach($power_unit_nums as $power_unit_num){
            array_push($power_units, [
                'id_power_unit' => $power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->id_power_unit,
                'power_unit_num' => $power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->power_unit_num,
                'description' => $power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->description,
                'id_corporation' => $power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->id_corporation,
                'id_location' => $power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->id_location,
                'id_power_unit_type' => $power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->id_power_unit_type,
                'is_active' => $power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->is_active,
                'insert_user' => $power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->insert_user,
                'insert_date' => $power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->insert_date,
                'update_user' => $power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->update_user,
                'update_date' => $power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->update_date,
            ]);
        }

        return response()->json([
            'success' => true,
            'data' => $power_units
        ], 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'power_unit_num' => 'required',
            'description' => 'required',
            'id_corporation' => 'required',
            'id_location' => 'required',
            'id_power_unit_type' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json([
                'message' => $validator->errors()
            ], 400);
        }

        try{
            PowerUnit::create([
                'power_unit_num' => $request->power_unit_num,
                'description' => $request->description,
                'id_corporation' => $request->id_corporation,
                'id_location' => $request->id_location,
                'id_power_unit_type' => $request->id_power_unit_type,
                'insert_user' => auth()->user()->id,
                'update_user' => auth()->user()->id,
            ]);

            return response()->json([
                'success' => true
            ], 200);
        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'power_unit_num' => 'required',
            'description' => 'required',
            'id_corporation' => 'required',
            'id_location' => 'required',
            'id_power_unit_type' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json([
                'message' => $validator->errors()
            ], 400);
        }

        try{
            PowerUnit::find($request->id_power_unit)->update([
                'power_unit_num' => $request->power_unit_num,
                'description' => $request->description,
                'id_corporation' => $request->id_corporation,
                'id_location' => $request->id_location,
                'id_power_unit_type' => $request->id_power_unit_type,
                'update_user' => auth()->user()->id,
            ]);
            return response()->json([
                'success' => true
            ], 200);
        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function destroy($id)
    {
        try{
            PowerUnit::find($id)->update([
                'is_active' => 'N'
            ]);
            return response()->json([
                'success' => true
            ], 200);
        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
