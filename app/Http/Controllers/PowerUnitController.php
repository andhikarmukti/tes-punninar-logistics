<?php

namespace App\Http\Controllers;

use App\Models\Corporation;
use App\Models\Location;
use App\Models\PowerUnit;
use App\Models\PowerUnitType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PowerUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Power Unit';
        $power_unit_nums = PowerUnit::where('is_active', 'Y')->pluck('power_unit_num')->unique();
        $power_unit = new PowerUnit;
        // $power_units = PowerUnit::orderBy('id_power_unit', 'ASC')->where('is_active', 'Y')->get();
        $location = new Location;

        $corporations = Corporation::all();
        $locations = Location::all();
        $power_unit_types = PowerUnitType::all();

        return view('power_unit.index', compact(
            'title',
            // 'power_units',
            'power_unit',
            'power_unit_nums',
            'location',
            'locations',
            'corporations',
            'power_unit_types'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'power_unit_num' => 'required',
            'description' => 'required',
            'id_corporation' => 'required',
            'id_location' => 'required',
            'id_power_unit_type' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json([
                'message' => $validator->errors()
            ], 400);
        }

        try{
            PowerUnit::create([
                'power_unit_num' => $request->power_unit_num,
                'description' => $request->description,
                'id_corporation' => $request->id_corporation,
                'id_location' => $request->id_location,
                'id_power_unit_type' => $request->id_power_unit_type,
                'insert_user' => auth()->user()->id,
                'update_user' => auth()->user()->id,
            ]);
        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PowerUnit  $powerUnit
     * @return \Illuminate\Http\Response
     */
    public function show(PowerUnit $powerUnit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PowerUnit  $powerUnit
     * @return \Illuminate\Http\Response
     */
    public function edit(PowerUnit $powerUnit, $id)
    {
        $title = 'Edit Power Unit';
        $power_unit = $powerUnit->find($id);

        $corporations = Corporation::all();
        $locations = Location::all();
        $power_unit_types = PowerUnitType::all();

        return view('power_unit.edit', compact(
            'title',
            'power_unit',
            'locations',
            'corporations',
            'power_unit_types'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PowerUnit  $powerUnit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'power_unit_num' => 'required',
            'description' => 'required',
            'id_corporation' => 'required',
            'id_location' => 'required',
            'id_power_unit_type' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json([
                'message' => $validator->errors()
            ], 400);
        }

        try{
            PowerUnit::find($request->id_power_unit)->update([
                'power_unit_num' => $request->power_unit_num,
                'description' => $request->description,
                'id_corporation' => $request->id_corporation,
                'id_location' => $request->id_location,
                'id_power_unit_type' => $request->id_power_unit_type,
                'update_user' => auth()->user()->id,
            ]);
        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PowerUnit  $powerUnit
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            PowerUnit::find($id)->update([
                'is_active' => 'N'
            ]);
        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }

    }

    public function soalNomor5()
    {
        $title = 'Soal Nomor 5';
        $power_units = PowerUnit::limit(3)->get();

        return view('power_unit.soalNomor5', compact(
            'title',
            'power_units'
        ));
    }
}
