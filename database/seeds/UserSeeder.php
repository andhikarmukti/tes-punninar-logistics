<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Andhika Raharja Mukti',
                'email' => 'andhikarmukti@gmail.com',
                'password' => Hash::make('asdasdasd')
            ],
            [
                'name' => 'Haura Razqya Mukti',
                'email' => 'haurarmukti@gmail.com',
                'password' => Hash::make('asdasdasd')
            ],
        ];

        foreach($users as $user){
            User::create([
                'name' => $user['name'],
                'email' => $user['email'],
                'password' => $user['password']
            ]);
        }
    }
}
