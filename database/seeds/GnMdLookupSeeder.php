<?php

use App\Models\GnMdLookup;
use Illuminate\Database\Seeder;

class GnMdLookupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gn_md_lookups = [
            [
                'lookup_id' => 1,
                'lookup_lines_code' => 'M',
                'description' => 'Male',
                'effective_from' => '2018/12/20',
                'effective_to' => '2020/12/20',
                'insert_user' => 1,
                'insert_time' => '2018/12/20',
            ],
            [
                'lookup_id' => 1,
                'lookup_lines_code' => 'F',
                'description' => 'Female',
                'effective_from' => '2018/12/20',
                'effective_to' => '2020/12/20',
                'insert_user' => 1,
                'insert_time' => '2018/12/20',
            ],
            [
                'lookup_id' => 2,
                'lookup_lines_code' => 'ISLAM',
                'description' => 'ISLAM',
                'effective_from' => '2018/12/01',
                'effective_to' => '2020/12/20',
                'insert_user' => 1,
                'insert_time' => '2018/12/21',
            ],
            [
                'lookup_id' => 2,
                'lookup_lines_code' => 'BUDHA',
                'description' => 'BUDHA',
                'effective_from' => '2018/12/01',
                'effective_to' => '2020/12/20',
                'insert_user' => 1,
                'insert_time' => '2018/12/20',
            ],
            [
                'lookup_id' => 2,
                'lookup_lines_code' => 'HINDU',
                'description' => 'HINDU',
                'effective_from' => '2018/12/01',
                'effective_to' => '2020/12/20',
                'insert_user' => 1,
                'insert_time' => '2018/12/20',
            ],
            [
                'lookup_id' => 3,
                'lookup_lines_code' => 'PRM_CLS_AR',
                'description' => 'Parameter Closing AR',
                'effective_from' => '2018/12/01',
                'effective_to' => '2020/12/31',
                'insert_user' => 1,
                'insert_time' => '2018/12/21',
            ],
        ];

        foreach($gn_md_lookups as $gn_md_lookup){
            GnMdLookup::create([
                'lookup_id' => $gn_md_lookup['lookup_id'],
                'lookup_lines_code' => $gn_md_lookup['lookup_lines_code'],
                'description' => $gn_md_lookup['description'],
                'effective_from' => $gn_md_lookup['effective_from'],
                'effective_to' => $gn_md_lookup['effective_to'],
                'insert_user' => $gn_md_lookup['insert_user'],
                'insert_time' => $gn_md_lookup['insert_time'],
            ]);
        }
    }
}
