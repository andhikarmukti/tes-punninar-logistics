<?php

use App\Models\GnMhLookup;
use Illuminate\Database\Seeder;

class GnMhLookupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gn_mh_lookups = [
            [
                'lookup_code' => 'MST_GENDER',
                'description' => 'Master Gender',
                'insert_user' => 1,
                'insert_time' => '2018/12/20'
            ],
            [
                'lookup_code' => 'MST_RELIGION',
                'description' => 'Master Religion',
                'insert_user' => 1,
                'insert_time' => '2018/12/20'
            ],
            [
                'lookup_code' => 'CLS_AR',
                'description' => 'Closing AR',
                'insert_user' => 1,
                'insert_time' => '2018/12/21'
            ],
        ];

        foreach($gn_mh_lookups as $gn_mh_lookup){
            GnMhLookup::create([
                'lookup_code' => $gn_mh_lookup['lookup_code'],
                'description' => $gn_mh_lookup['description'],
                'insert_user' => $gn_mh_lookup['insert_user'],
                'insert_time' => $gn_mh_lookup['insert_time']
            ]);
        }
    }
}
