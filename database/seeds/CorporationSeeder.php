<?php

use App\Models\Corporation;
use Illuminate\Database\Seeder;

class CorporationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $corporations = [
            [
                'corporation_name' => 'PT PUNINAR JAYA',
                'insert_user' => 1,
                'insert_user' => 1,
                'insert_date' => '2018/12/20',
                'update_user' => 2,
                'update_date' => '2018/12/21',
            ],
            [
                'corporation_name' => 'PT PUNINAR INFINITE RAYA',
                'insert_user' => 1,
                'insert_user' => 1,
                'insert_date' => '2018/12/20',
                'update_user' => 2,
                'update_date' => '2018/12/21',
            ],
        ];

        foreach($corporations as $corporation){
            Corporation::create([
                'corporation_name' => $corporation['corporation_name'],
                'insert_user' => $corporation['insert_user'],
                'insert_date' => $corporation['insert_date'],
                'update_user' => $corporation['update_user'],
                'update_date' => $corporation['update_date'],
            ]);
        }
    }
}
