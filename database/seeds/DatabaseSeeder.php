<?php

use GnMdLookupSeeder as GnMdLookupSeeder;
use UserSeeder as UserSeeder;
use Illuminate\Database\Seeder;
use LocationSeeder as LocationSeeder;
use PowerUnitSeeder as PowerUnitSeeder;
use GnMhLookupSeeder as GnMhLookupSeeder;
use CorporationSeeder as CorporationSeeder;
use PowerUnitTypeSeeder as PowerUnitTypeSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            PowerUnitSeeder::class,
            CorporationSeeder::class,
            LocationSeeder::class,
            PowerUnitTypeSeeder::class,
            GnMhLookupSeeder::class,
            GnMdLookupSeeder::class
        ]);
    }
}
