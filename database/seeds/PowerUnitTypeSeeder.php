<?php

use App\Models\PowerUnitType;
use Illuminate\Database\Seeder;

class PowerUnitTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power_unit_types = [
            [
                'power_unit_type_xid' => 'ENGKEL',
                'description' => 'PRIME_MOVER ENGKEL NON KAROSERI',
                'insert_user' => 1,
                'insert_date' => '2018/12/20',
                'update_user' => 2,
                'update_date' => '2018/12/21',
            ],
            [
                'power_unit_type_xid' => 'TRONTON',
                'description' => 'PRIME_MOVER TRONTON NON KAROSERI',
                'insert_user' => 1,
                'insert_date' => '2018/12/20',
                'update_user' => 2,
                'update_date' => '2018/12/21',
            ],
            [
                'power_unit_type_xid' => 'TOWING_CDD',
                'description' => 'RIGID CDD JENIS KAROSERI TOWING',
                'insert_user' => 1,
                'insert_date' => '2018/12/20',
                'update_user' => 2,
                'update_date' => '2018/12/21',
            ],
            [
                'power_unit_type_xid' => 'CAR-CARRIER_ENGKEL',
                'description' => 'PRIME MOVER ENGKEL JENIS KAROSERI CAR CARRIER',
                'insert_user' => 1,
                'insert_date' => '2018/12/20',
                'update_user' => 2,
                'update_date' => '2018/12/21',
            ],
            [
                'power_unit_type_xid' => 'MOTOR-CARRIER_ENGKEL',
                'description' => 'PRIME MOVER ENGKEL JENIS KAROSERI MOTOR CARRIER',
                'insert_user' => 1,
                'insert_date' => '2018/12/20',
                'update_user' => 2,
                'update_date' => '2018/12/21',
            ],
            [
                'power_unit_type_xid' => 'FLAT-DECK_TRONTON',
                'description' => 'RIGID TRONTON JENIS KAROSERI FLAT DECK',
                'insert_user' => 1,
                'insert_date' => '2018/12/20',
                'update_user' => 2,
                'update_date' => '2018/12/21',
            ],
            [
                'power_unit_type_xid' => 'MIXER_TRONTON',
                'description' => 'RIGID TRONTON JENIS KAROSERI MIXER',
                'insert_user' => 1,
                'insert_date' => '2018/12/20',
                'update_user' => 2,
                'update_date' => '2018/12/21',
            ],
        ];

        foreach($power_unit_types as $power_unit_type){
            PowerUnitType::create([
                'power_unit_type_xid' => $power_unit_type['power_unit_type_xid'],
                'description' => $power_unit_type['description'],
                'insert_user' => $power_unit_type['insert_user'],
                'insert_date' => $power_unit_type['insert_date'],
                'update_user' => $power_unit_type['update_user'],
                'update_date' => $power_unit_type['update_date'],
            ]);
        }
    }
}
