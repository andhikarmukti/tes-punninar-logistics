<?php

use App\Models\Location;
use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locations = [
            [
                'location_name' => 'POOL CILEGON',
                'city' => 'Cilegon',
                'province' => 'Banten',
                'latitude' => '-6,002534',
                'longitude' => '106,011124',
                'insert_user' => 1,
                'insert_date' => '2018/12/20',
                'update_user' => 2,
                'update_date' => '2018/12/21',
            ],
            [
                'location_name' => 'SURABAYA POOL',
                'city' => 'Surabaya',
                'province' => 'Jawa Timur',
                'latitude' => '-7,6503',
                'longitude' => '112,7057',
                'insert_user' => 1,
                'insert_date' => '2018/12/20',
                'update_user' => 2,
                'update_date' => '2018/12/21',
            ],
            [
                'location_name' => 'POOL CAKUNG',
                'city' => 'Cakung',
                'province' => 'DKI Jakarta',
                'latitude' => '-6,172035',
                'longitude' => '106,942108',
                'insert_user' => 1,
                'insert_date' => '2018/12/20',
                'update_user' => 2,
                'update_date' => '2018/12/21',
            ],
            [
                'location_name' => 'POOL NAGRAK',
                'city' => 'Nagrak',
                'province' => 'DKI Jakarta',
                'latitude' => '-6,116907',
                'longitude' => '106,942471',
                'insert_user' => 1,
                'insert_date' => '2018/12/20',
                'update_user' => 2,
                'update_date' => '2018/12/21',
            ],
            [
                'location_name' => 'POOL MEDAN',
                'city' => 'Medan',
                'province' => 'Sumatra Utara',
                'latitude' => '3,590000',
                'longitude' => '98,67802',
                'insert_user' => 1,
                'insert_date' => '2018/12/20',
                'update_user' => 2,
                'update_date' => '2018/12/21',
            ],
        ];

        foreach($locations as $location){
            Location::create([
                'location_name' => $location['location_name'],
                'city' => $location['city'],
                'province' => $location['province'],
                'latitude' => $location['latitude'],
                'longitude' => $location['longitude'],
                'insert_user' => $location['insert_user'],
                'insert_date' => $location['insert_date'],
                'update_user' => $location['update_user'],
                'update_date' => $location['update_date'],
            ]);
        }
    }
}
