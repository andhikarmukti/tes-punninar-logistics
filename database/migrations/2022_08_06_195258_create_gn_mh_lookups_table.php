<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGnMhLookupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gn_mh_lookups', function (Blueprint $table) {
            $table->id('lookup_id');
            $table->string('lookup_code');
            $table->string('description');
            $table->foreignId('insert_user');
            $table->date('insert_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gn_mh_lookups');
    }
}
