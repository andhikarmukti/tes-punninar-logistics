<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGnMdLookupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gn_md_lookups', function (Blueprint $table) {
            $table->id('lookup_lines_id');
            $table->foreignId('lookup_id');
            $table->string('lookup_lines_code');
            $table->string('description');
            $table->date('effective_from');
            $table->date('effective_to');
            $table->foreignId('insert_user');
            $table->date('insert_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gn_md_lookups');
    }
}
