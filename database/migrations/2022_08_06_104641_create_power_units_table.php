<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePowerUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('power_units', function (Blueprint $table) {
            $table->id('id_power_unit');
            $table->string('power_unit_num');
            $table->string('description');
            $table->foreignId('id_corporation');
            $table->foreignId('id_location');
            $table->foreignId('id_power_unit_type');
            $table->enum('is_active', ['Y', 'N'])->default('Y');
            $table->foreignId('insert_user');
            $table->timestamp('insert_date');
            $table->foreignId('update_user');
            $table->timestamp('update_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('power_units');
    }
}
