<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePowerUnitTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('power_unit_types', function (Blueprint $table) {
            $table->id('id_power_unit_type');
            $table->string('power_unit_type_xid');
            $table->string('description');
            $table->foreignId('insert_user');
            $table->timestamp('insert_date');
            $table->foreignId('update_user');
            $table->timestamp('update_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('power_unit_types');
    }
}
