<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->id('id_location');
            $table->string('location_name');
            $table->string('city');
            $table->string('province');
            $table->string('latitude', 10, 7);
            $table->string('longitude', 10, 7);
            $table->foreignId('insert_user');
            $table->timestamp('insert_date');
            $table->foreignId('update_user');
            $table->timestamp('update_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
