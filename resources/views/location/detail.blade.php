@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <a href="/location" class="btn btn-primary mt-3">Back</a>
                <div class="card shadow-sm p-4 mt-4">
                    <div class="card-body">
                        <h5 class="card-title">{{ $detail_location->location_name }}</h5>
                        <a target="_blank"
                            href="https://maps.google.com/maps/place/{{ str_replace(',', '.', $detail_location->latitude) . ',' . str_replace(',', '.', $detail_location->longitude) }}"
                            class="text-decoration-none badge text-bg-warning">open map</a>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Jumlah Truk Saat Ini : <h5 class="d-inline">{{ count($total_power_units) }}</h5></li>
                        @php
                            $i = 0;
                        @endphp
                        <div class="accordion accordion-flush" id="accordionFlushExample">
                            @foreach ($total_power_units as $power_unit)
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="flush-headingOne">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#flush-collapse{{ $i }}" aria-expanded="false"
                                            aria-controls="flush-collapseOne">
                                            {{ $power_unit->power_unit_num }}
                                        </button>
                                    </h2>
                                    <div id="flush-collapse{{ $i }}" class="accordion-collapse collapse"
                                        aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Description : <h6 class="d-inline">{{ $power_unit->description }}</h6></li>
                                                <li>Corporation : <h6 class="d-inline">{{ $power_unit->corporation->corporation_name }}</h6></li>
                                                <li>Current Location : <h6 class="d-inline">{{ $power_unit->location->city }}</h6></li>
                                                <li>Power Unit Type : <h6 class="d-inline">{{ $power_unit->powerUnitType->power_unit_type_xid }}</h6>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                @php
                                    $i++;
                                @endphp
                            @endforeach
                        </div>
                    </ul>
                </div>
            </div>
        </div>
    @endsection
