@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h1>Halaman Location</h1>
        <div class="col">
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Location Name</th>
                    <th scope="col">City</th>
                    <th scope="col">Province</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($locations as $location)
                        <tr>
                            <th scope="row">{{ $location->id_location }}</th>
                            <td>{{ $location->location_name }}</td>
                            <td>{{ $location->city }}</td>
                            <td>{{ $location->province }}</td>
                            <td>
                                <a href="/location/{{ $location->id_location }}" class="badge bg-primary text-decoration-none detail">detail</a>
                                <a target="_blank" href="https://maps.google.com/maps/place/{{ str_replace(',', '.', $location->latitude) . ',' . str_replace(',', '.', $location->longitude) }}" class="text-decoration-none badge text-bg-warning">open map</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
        </div>
    </div>
</div>
@endsection
