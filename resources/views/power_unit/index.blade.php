@extends('layouts.app')

@section('content')
    <div class="container pb-5">
        <h1 class="text-center mb-5 mt-3">Halaman Power Unit</h1>
        <button class="btn btn-primary mt-2 mb-3" id="buttonTambahData">Tambah Data</button>
        <div class="col col-md-6 mt-1 mb-5" id="divFormTambahData" hidden>
            <div class="card shadow-sm p-3">
                <form id="formTambahData">
                    @csrf
                    <div class="mb-3">
                        <label for="power_unit_num" class="form-label">Power Unit Num</label>
                        <input type="text" class="form-control" id="power_unit_num" name="power_unit_num"
                            autocomplete="off">
                        <small class="invalid-feedback" id="powerUnitError"></small>
                    </div>
                    <div class="mb-3">
                        <label for="description" class="form-label">Description</label>
                        <input type="text" class="form-control" id="description" name="description" autocomplete="off">
                        <small class="invalid-feedback" id="descriptionError"></small>
                    </div>
                    <div class="mb-3">
                        <label for="id_corporation" class="form-label">Corporation</label>
                        <select class="form-select" name="id_corporation" id="id_corporation">
                            <option value="" selected disabled>Select Corporation</option>
                            @foreach ($corporations as $corporation)
                                <option value="{{ $corporation->id_corporation }}">{{ $corporation->corporation_name }}
                                </option>
                            @endforeach
                        </select>
                        <small class="invalid-feedback" id="id_corporationError"></small>
                    </div>
                    <div class="mb-3">
                        <label for="id_location" class="form-label">Location</label>
                        <select class="form-select" name="id_location" id="id_location">
                            <option value="" selected disabled>Select Location</option>
                            @foreach ($locations as $location)
                                <option value="{{ $location->id_location }}">{{ $location->location_name }}</option>
                            @endforeach
                        </select>
                        <small class="invalid-feedback" id="id_locationError"></small>
                    </div>
                    <div class="mb-3">
                        <label for="id_power_unit_type" class="form-label">Power Unit Type</label>
                        <select class="form-select" name="id_power_unit_type" id="id_power_unit_type">
                            <option value="" selected disabled>Select Power Unit Type</option>
                            @foreach ($power_unit_types as $power_unit_type)
                                <option value="{{ $power_unit_type->id_power_unit_type }}">
                                    {{ $power_unit_type->power_unit_type_xid }}</option>
                            @endforeach
                        </select>
                        <small class="invalid-feedback" id="id_power_unit_typeError"></small>
                    </div>
                    <button type="button" class="btn btn-primary d-flex ms-auto" onclick="handleSubmit()">Submit</button>
                </form>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col">
                <div class="card shadow-sm p-4">
                    <table class="table table-hover" id="tablePowerUnit">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Power Unit Num</th>
                                <th scope="col">Description</th>
                                <th scope="col">Corporation</th>
                                <th scope="col">Current Location</th>
                                <th scope="col">Power Unit Type</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach ($power_unit_nums as $power_unit_num)
                                <tr>
                                    <th scope="row">
                                        {{ $i++ }}
                                    </th>
                                    <th>{{ $power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->power_unit_num }}
                                    </th>
                                    <td>{{ $power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->description }}
                                    </td>
                                    <td>{{ $power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->corporation->corporation_name }}
                                    </td>
                                    <td><a target="_blank"
                                            href="https://maps.google.com/maps/place/{{ str_replace(',','.',$location->find($power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->id_location)->latitude) .',' .str_replace(',','.',$location->find($power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->id_location)->longitude) }}"
                                            class="text-decoration-none">{{ $power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->location->location_name }}</a>
                                    </td>
                                    <td>{{ $power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->powerUnitType->power_unit_type_xid }}
                                    </td>
                                    <td class="d-flex align-items-center gap-1">
                                        <a class="badge text-bg-warning text-decoration-none"
                                            href="/power-unit/{{ $power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->id_power_unit }}">edit</a>
                                        <button
                                            data-id="{{ $power_unit->latest()->where('power_unit_num', $power_unit_num)->first()->id_power_unit }}"
                                            type="submit"
                                            class="badge text-bg-danger border-0 buttonDelete">delete</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#tablePowerUnit').DataTable({
                order: [
                    [0, "desc"]
                ],
                responsive: true,
                buttons: ['print', 'excel', 'pdf'],
                dom: "<'row'<'col-md-3'l><'col-md-6'B><'col-md-3'f>>" +
                    "<'row'<'col-md-12'tr>>" +
                    "<'row'<'col-md-5'i><'col-md-7'p>>",
                lengthMenu: [
                    [10, 15, 25, 50, -1],
                    [10, 15, 25, 50, "All"]
                ],
                columnDefs: [{
                        className: "dt-head-center",
                        targets: [0, 1, 2, 3, 4, 5]
                    },
                    {
                        className: "dt-body-center",
                        targets: [0, 1, 2, 3, 4, 5]
                    }
                ]
            });
        });
    </script>
    <script>
        const buttonTambahData = $('#buttonTambahData');
        const divFormTambahData = $('#divFormTambahData');
        const formTambahData = $('#formTambahData')

        $('.buttonDelete').click(function() {
            const id = $(this).data('id');
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "/power-unit/" + id,
                        method: 'DELETE',
                        data: {
                            '_token': "{{ csrf_token() }}",
                        },
                        success: (res) => {
                            Swal.fire({
                                icon: 'success',
                                text: 'Berhasil hapus data',
                                timer: 3000
                            }).then(() => {
                                location.reload(true);
                            })
                        }
                    })
                }
            })
        })

        buttonTambahData.click(() => {
            $(this).attr('hidden', true);
            divFormTambahData.removeAttr('hidden');
        });

        const clearError = () => {
            $('#powerUnitError').text('');
            $('#power_unit_num').removeClass('is-invalid');
            $('#descriptionError').text('');
            $('#description').removeClass('is-invalid');
            $('#id_corporationError').text('');
            $('#id_corporation').removeClass('is-invalid');
            $('#id_locationError').text('');
            $('#id_location').removeClass('is-invalid');
            $('#id_power_unit_typeError').text('');
            $('#id_power_unit_type').removeClass('is-invalid');
        }

        const clearInput = () => {
            $('#power_unit_num').val('');
            $('#description').val('');
            $('#id_corporation').val('');
            $('#id_location').val('');
            $('#id_power_unit_type').val('');
        }

        const handleSubmit = () => {
            const data = formTambahData.serializeArray().reduce(function(obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {});

            $.ajax({
                url: "{{ route('power-unit.store') }}",
                data: data,
                method: 'POST',
                success: (res) => {
                    clearError();
                    clearInput();
                    if (!res) {
                        Swal.fire({
                            title: 'Berhasil',
                            text: "Berhasil menambahkan data",
                            icon: 'success',
                            timer: 3000
                        }).then(() => {
                            location.reload(true);
                        });
                    }
                },
                error: (err) => {
                    console.log(err.responseJSON.message);
                    if (err.responseJSON.message.power_unit_num) {
                        $('#powerUnitError').text(err.responseJSON.message.power_unit_num[0]);
                        $('#power_unit_num').addClass('is-invalid');
                    } else {
                        $('#powerUnitError').text('');
                        $('#power_unit_num').removeClass('is-invalid');
                    }

                    if (err.responseJSON.message.description) {
                        $('#descriptionError').text(err.responseJSON.message.description[0]);
                        $('#description').addClass('is-invalid');
                    } else {
                        $('#descriptionError').text('');
                        $('#description').removeClass('is-invalid');
                    }

                    if (err.responseJSON.message.id_corporation) {
                        $('#id_corporationError').text(err.responseJSON.message.id_corporation[0]);
                        $('#id_corporation').addClass('is-invalid');
                    } else {
                        $('#id_corporationError').text('');
                        $('#id_corporation').removeClass('is-invalid');
                    }

                    if (err.responseJSON.message.id_location) {
                        $('#id_locationError').text(err.responseJSON.message.id_location[0]);
                        $('#id_location').addClass('is-invalid');
                    } else {
                        $('#id_locationError').text('');
                        $('#id_location').removeClass('is-invalid');
                    }

                    if (err.responseJSON.message.id_power_unit_type) {
                        $('#id_power_unit_typeError').text(err.responseJSON.message.id_power_unit_type[0]);
                        $('#id_power_unit_type').addClass('is-invalid');
                    } else {
                        $('#id_power_unit_typeError').text('');
                        $('#id_power_unit_type').removeClass('is-invalid');
                    }
                }
            })
        }
    </script>
@endsection
