@extends('layouts.app')

@section('content')
    <div class="container pb-5">
        <h1 class="text-center mb-5 mt-3">Edit Power Unit</h1>
        <div class="col-6 mt-1 mb-5" id="divFormTambahData">
            <div class="card shadow-sm p-3">
                <form id="formTambahData">
                    @csrf
                    <div class="mb-3">
                        <label for="power_unit_num" class="form-label">Power Unit Num</label>
                        <input value="{{ $power_unit->power_unit_num }}" type="text" class="form-control" id="power_unit_num" name="power_unit_num"
                            autocomplete="off">
                            <small class="invalid-feedback" id="powerUnitError"></small>
                    </div>
                    <div class="mb-3">
                        <label for="description" class="form-label">Description</label>
                        <input value="{{ $power_unit->description }}" type="text" class="form-control" id="description" name="description" autocomplete="off">
                        <small class="invalid-feedback" id="descriptionError"></small>
                    </div>
                    <div class="mb-3">
                        <label for="id_corporation" class="form-label">Corporation</label>
                        <select class="form-select" name="id_corporation" id="id_corporation">
                            <option value="" selected disabled>Select Corporation</option>
                            @foreach ($corporations as $corporation)
                                <option value="{{ $corporation->id_corporation }}" {{ $power_unit->id_corporation == $corporation->id_corporation ? 'selected' : '' }}>{{ $corporation->corporation_name }}</option>
                            @endforeach
                        </select>
                        <small class="invalid-feedback" id="id_corporationError"></small>
                    </div>
                    <div class="mb-3">
                        <label for="id_location" class="form-label">Location</label>
                        <select class="form-select" name="id_location" id="id_location">
                            <option value="" selected disabled>Select Location</option>
                            @foreach ($locations as $location)
                                <option value="{{ $location->id_location }}" {{ $power_unit->id_location == $location->id_location ? 'selected' : '' }}>{{ $location->location_name }}</option>
                            @endforeach
                        </select>
                        <small class="invalid-feedback" id="id_locationError"></small>
                    </div>
                    <div class="mb-3">
                        <label for="id_power_unit_type" class="form-label">Power Unit Type</label>
                        <select class="form-select" name="id_power_unit_type" id="id_power_unit_type">
                            <option value="" selected disabled>Select Power Unit Type</option>
                            @foreach ($power_unit_types as $power_unit_type)
                                <option value="{{ $power_unit_type->id_power_unit_type }}" {{ $power_unit->id_power_unit_type == $power_unit_type->id_power_unit_type ? 'selected' : '' }}>{{ $power_unit_type->power_unit_type_xid }}</option>
                            @endforeach
                        </select>
                        <small class="invalid-feedback" id="id_power_unit_typeError"></small>
                    </div>
                    <input type="hidden" value="{{ $power_unit->id_power_unit }}" name="id_power_unit" >
                    <button type="button" class="btn btn-primary d-flex ms-auto" onclick="handleEdit()">Edit</button>
                </form>
            </div>
        </div>
    </div>

    <script>
        const formTambahData = $('#formTambahData')

        const clearError = () => {
            $('#powerUnitError').text('');
            $('#power_unit_num').removeClass('is-invalid');
            $('#descriptionError').text('');
            $('#description').removeClass('is-invalid');
            $('#id_corporationError').text('');
            $('#id_corporation').removeClass('is-invalid');
            $('#id_locationError').text('');
            $('#id_location').removeClass('is-invalid');
            $('#id_power_unit_typeError').text('');
            $('#id_power_unit_type').removeClass('is-invalid');
        }

        const clearInput = () => {
            $('#power_unit_num').val('');
            $('#description').val('');
            $('#id_corporation').val('');
            $('#id_location').val('');
            $('#id_power_unit_type').val('');
        }

        const handleEdit = () => {
            const data = formTambahData.serializeArray().reduce(function(obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {});

            $.ajax({
                url : "{{ route('power-unit.update') }}",
                data : data,
                method : 'PUT',
                success : (res) => {
                    console.log(res);
                    clearError();
                    clearInput();
                    if(!res){
                        Swal.fire({
                            title: 'Berhasil',
                            text: "Berhasil merubah data",
                            icon: 'success',
                            timer : 3000
                        }).then(() => {
                            location.replace('/power-unit');
                        });
                    }
                },
                error : (err) => {
                    console.log(err.responseJSON.message);
                    if(err.responseJSON.message.power_unit_num){
                        $('#powerUnitError').text(err.responseJSON.message.power_unit_num[0]);
                        $('#power_unit_num').addClass('is-invalid');
                    }else{
                        $('#powerUnitError').text('');
                        $('#power_unit_num').removeClass('is-invalid');
                    }

                    if(err.responseJSON.message.description){
                        $('#descriptionError').text(err.responseJSON.message.description[0]);
                        $('#description').addClass('is-invalid');
                    }else{
                        $('#descriptionError').text('');
                        $('#description').removeClass('is-invalid');
                    }

                    if(err.responseJSON.message.id_corporation){
                        $('#id_corporationError').text(err.responseJSON.message.id_corporation[0]);
                        $('#id_corporation').addClass('is-invalid');
                    }else{
                        $('#id_corporationError').text('');
                        $('#id_corporation').removeClass('is-invalid');
                    }

                    if(err.responseJSON.message.id_location){
                        $('#id_locationError').text(err.responseJSON.message.id_location[0]);
                        $('#id_location').addClass('is-invalid');
                    }else{
                        $('#id_locationError').text('');
                        $('#id_location').removeClass('is-invalid');
                    }

                    if(err.responseJSON.message.id_power_unit_type){
                        $('#id_power_unit_typeError').text(err.responseJSON.message.id_power_unit_type[0]);
                        $('#id_power_unit_type').addClass('is-invalid');
                    }else{
                        $('#id_power_unit_typeError').text('');
                        $('#id_power_unit_type').removeClass('is-invalid');
                    }
                }
            })
        }
    </script>
@endsection
