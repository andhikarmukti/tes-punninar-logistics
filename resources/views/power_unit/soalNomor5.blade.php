@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col mt-5">
                <table class="table table-hover border mt-5" id="tableSoalNomor5">
                    <thead class="text-bg-dark">
                      <tr>
                        <th scope="col">POWER_UNIT_NUM</th>
                        <th scope="col">DESCRIPTION</th>
                        <th scope="col">ID_CORPORATION</th>
                        <th scope="col">ID_LOCATION</th>
                        <th scope="col">ID_POWER_UNIT_TYPE</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($power_units as $power_unit)
                        <tr>
                          <th scope="row">{{ $power_unit->power_unit_num }}</th>
                          <td>{{ $power_unit->description }}</td>
                          <td>{{ $power_unit->corporation->corporation_name }}</td>
                          <td>{{ $power_unit->location->location_name }}</td>
                          <td>{{ $power_unit->powerUnitType->power_unit_type_xid }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#tableSoalNomor5').DataTable({
                order: [
                    [0, "desc"]
                ],
                responsive: true,
                buttons: ['print', 'excel', 'pdf'],
                dom: "<'row'<'col-md-3'l><'col-md-6'B><'col-md-3'f>>" +
                    "<'row'<'col-md-12'tr>>" +
                    "<'row'<'col-md-5'i><'col-md-7'p>>",
                lengthMenu: [
                    [10, 15, 25, 50, -1],
                    [10, 15, 25, 50, "All"]
                ],
                columnDefs: [{
                        className: "dt-head-center",
                        targets: [0, 1, 2, 3, 4]
                    },
                    {
                        className: "dt-body-center",
                        targets: [0, 1, 2, 3, 4]
                    }
                ]
            });
        });
    </script>
@endsection
