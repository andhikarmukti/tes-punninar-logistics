<nav class="navbar navbar-expand-lg bg-light shadow-sm">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">Home</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link {{ request()->is('power-unit*') ? 'active' : '' }}" aria-current="page" href="/power-unit">Power Unit</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ request()->is('location*') ? 'active' : '' }}" aria-current="page" href="/location">Location</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ request()->is('soal-nomor-5*') ? 'active' : '' }}" aria-current="page" href="/soal-nomor-5">Soal Nomor 5</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle {{ request()->is('master-lookup*') ? 'active' : '' }}" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Master Lookup
            </a>
            <ul class="dropdown-menu">
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('master-lookup/gn-mh*') ? 'active' : '' }}" aria-current="page" href="/master-lookup/gn-mh">GN MH</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('master-lookup/gn-md*') ? 'active' : '' }}" aria-current="page" href="/master-lookup/gn-md">GN MD</a>
                </li>
            </ul>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle {{ request()->is('master-lookup*') ? 'active' : '' }}" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Soal Nomor 7
            </a>
            <ul class="dropdown-menu">
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('soal-nomor-7*') ? 'active' : '' }}" aria-current="page" href="/soal-nomor-7-a">7 A</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('soal-nomor-7*') ? 'active' : '' }}" aria-current="page" href="/soal-nomor-7-b">7 B</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('soal-nomor-7*') ? 'active' : '' }}" aria-current="page" href="/soal-nomor-7-c">7 C</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('soal-nomor-7*') ? 'active' : '' }}" aria-current="page" href="/soal-nomor-7-d">7 D</a>
                </li>
            </ul>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Profile
            </a>
            <ul class="dropdown-menu">
                <form action="/logout" method="post">
                    @csrf
                    <li><button type="submit" class="dropdown-item">Logout</button></li>
                </form>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </nav>
