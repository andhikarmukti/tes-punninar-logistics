@extends('layouts.app')

@section('content')
    <div class="container p-md-0 p-4">
        <div class="row justify-content-center">
            <h1 class="text-center mb-5 mt-3">Halaman GN MH</h1>
            <div class="col col-md-4 card shadow-sm p-3">
                @if(session()->has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                @endif
                @if(session()->has('edit'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    {{ session('edit') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                @endif
                @if(session()->has('delete'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ session('delete') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                @endif
                <form action="/master-lookup/gn-mh" method="post">
                    @csrf
                    <div class="mb-3">
                      <label for="lookup_code" class="form-label">Lookup Code</label>
                      <input type="text" class="form-control @error('lookup_code') is-invalid @enderror" id="lookup_code" name="lookup_code" autocomplete="off">
                      @error('lookup_code')
                        <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>
                    <div class="mb-3">
                      <label for="description" class="form-label">Description</label>
                      <input type="text" class="form-control @error('description') is-invalid @enderror" id="description" name="description" autocomplete="off">
                      @error('description')
                        <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>
                    <button type="submit" class="btn btn-primary d-flex ms-auto">Submit</button>
                  </form>
            </div>
            <div class="card shadow-sm p-4 mt-5 mb-5">
                <table class="table table-hover" id="tableGnMh">
                    <thead class="text-bg-dark">
                      <tr>
                        <th scope="col">Lookup Code</th>
                        <th scope="col">Description</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($gn_mhs as $gn_mh)
                        <tr>
                          <th scope="row">{{ $gn_mh->lookup_code }}</th>
                          <td>{{ $gn_mh->description }}</td>
                          <td>
                            <a href="/master-lookup/gn-mh/{{ $gn_mh->lookup_id }}" class="badge text-bg-warning text-decoration-none">edit</a>
                            <a href="#" data-id="{{ $gn_mh->lookup_id }}" class="badge text-bg-danger text-decoration-none delete">delete</a>
                          </td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#tableGnMh').DataTable({
                responsive: true,
                buttons: ['print', 'excel', 'pdf'],
                dom: "<'row'<'col-md-3'l><'col-md-6'B><'col-md-3'f>>" +
                    "<'row'<'col-md-12'tr>>" +
                    "<'row'<'col-md-5'i><'col-md-7'p>>",
                lengthMenu: [
                    [5, 10, 15, 25, 50, -1],
                    [5, 10, 15, 25, 50, "All"]
                ],
                columnDefs: [{
                        className: "dt-head-center",
                        targets: [0, 1, 2]
                    },
                    {
                        className: "dt-body-center",
                        targets: [0, 1, 2]
                    }
                ]
            });
        });
    </script>

    <script>
        $('.delete').click(function(){
            const id = $(this).data('id');
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "/master-lookup/gn-mh/" + id,
                        method: 'DELETE',
                        data: {
                            '_token': "{{ csrf_token() }}",
                        },
                        success: (res) => {
                            console.log(res);
                            Swal.fire({
                                icon: 'success',
                                text: 'Berhasil hapus data',
                                timer: 3000
                            }).then(() => {
                                location.reload(true);
                            })
                        }
                    })
                }
            })
        });
    </script>
@endsection
