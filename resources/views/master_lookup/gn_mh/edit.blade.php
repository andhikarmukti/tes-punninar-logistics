@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="text-center mb-5 mt-3">Halaman Edit GN MH</h1>
        <div class="row justify-content-center">
            <div class="col col-md-4">
                <a href="/master-lookup/gn-mh" class="btn btn-primary mb-3">Back</a>
                <div class="card shadow-sm p-3">
                    @if(session()->has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('success') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                    <form action="/master-lookup/gn-mh/{{ $gn_mh->lookup_id }}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="mb-3">
                          <label for="lookup_code" class="form-label">Lookup Code</label>
                          <input value="{{ old('lookup_code') ?? $gn_mh->lookup_code }}" type="text" class="form-control @error('lookup_code') is-invalid @enderror" id="lookup_code" name="lookup_code" autocomplete="off">
                          @error('lookup_code')
                            <span class="text-danger">{{ $message }}</span>
                          @enderror
                        </div>
                        <div class="mb-3">
                          <label for="description" class="form-label">Description</label>
                          <input value="{{ old('description') ?? $gn_mh->description }}" type="text" class="form-control @error('description') is-invalid @enderror" id="description" name="description" autocomplete="off">
                          @error('description')
                            <span class="text-danger">{{ $message }}</span>
                          @enderror
                        </div>
                        <button type="submit" class="btn btn-primary d-flex ms-auto">Edit</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
@endsection
