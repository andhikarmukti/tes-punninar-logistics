@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <h1 class="text-center mb-4">Halaman Gn Md</h1>
            <div class="col col-md-6">
                <div class="card shadow-sm p-4">
                    @if(session()->has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('success') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                    @if(session()->has('update'))
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        {{ session('update') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                    <form action="/master-lookup/gn-md" method="POST">
                        @csrf
                        <div class="mb-3">
                            <label for="lookup_id" class="form-label">Gn Mh Lookup</label>
                            <select name="lookup_id" id="lookup_id" class="form-select @error('lookup_id') is-invalid @enderror">
                                <option value="" selected disabled>Select Gn Mh Lookup</option>
                                @foreach ($gn_mhs as $gn_mh)
                                    <option value="{{ $gn_mh->lookup_id }}">{{ $gn_mh->lookup_code }}</option>
                                @endforeach
                            </select>
                            @error('lookup_id')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="lookup_lines_code" class="form-label">Lookup Lines Code</label>
                            <input type="text" class="form-control @error('lookup_lines_code') is-invalid @enderror" id="lookup_lines_code" name="lookup_lines_code">
                            @error('lookup_lines_code')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="description" class="form-label">Description</label>
                            <input type="text" class="form-control @error('description') is-invalid @enderror" id="description" name="description">
                            @error('description')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="effective_from" class="form-label">Effective From</label>
                            <input type="date" class="form-control @error('effective_from') is-invalid @enderror" id="effective_from" name="effective_from">
                            @error('effective_from')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="effective_to" class="form-label">Effective To</label>
                            <input type="date" class="form-control @error('effective_to') is-invalid @enderror" id="effective_to" name="effective_to">
                            @error('effective_to')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary d-flex ms-auto">Submit</button>
                    </form>
                </div>
            </div>
            <div class="card shadow-sm p-4 mt-4 mb-5">
                <table class="table table-hover" id="tableGnMd">
                    <thead>
                      <tr>
                        <th scope="col">Lookup ID</th>
                        <th scope="col">Look Up Lines Code</th>
                        <th scope="col">Description</th>
                        <th scope="col">Effective From</th>
                        <th scope="col">Effective To</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($gn_mds as $gn_md)
                            <tr>
                                <th scope="row">{{ $gn_md->lookup_id }}</th>
                                <td>{{ $gn_md->lookup_lines_code }}</td>
                                <td>{{ $gn_md->description }}</td>
                                <td>{{ $gn_md->effective_from }}</td>
                                <td>{{ $gn_md->effective_to }}</td>
                                <td>
                                    <a href="/master-lookup/gn-md/{{ $gn_md->lookup_lines_id }}" class="badge text-bg-warning text-decoration-none">edit</a>
                                    <a href="#" data-id="{{ $gn_md->lookup_lines_id }}" class="badge text-bg-danger text-decoration-none delete">delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                  </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#tableGnMd').DataTable({
                responsive: true,
                buttons: ['print', 'excel', 'pdf'],
                dom: "<'row'<'col-md-3'l><'col-md-6'B><'col-md-3'f>>" +
                    "<'row'<'col-md-12'tr>>" +
                    "<'row'<'col-md-5'i><'col-md-7'p>>",
                lengthMenu: [
                    [5, 10, 15, 25, 50, -1],
                    [5, 10, 15, 25, 50, "All"]
                ],
                columnDefs: [{
                        className: "dt-head-center",
                        targets: [0, 1, 2, 3, 4, 5]
                    },
                    {
                        className: "dt-body-center",
                        targets: [0, 1, 2, 3, 4, 5]
                    }
                ]
            });
        });
    </script>

    <script>
        $('.delete').click(function(){
            const id = $(this).data('id');
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "/master-lookup/gn-md/" + id,
                        method: 'DELETE',
                        data: {
                            '_token': "{{ csrf_token() }}",
                        },
                        success: (res) => {
                            console.log(res);
                            Swal.fire({
                                icon: 'success',
                                text: 'Berhasil hapus data',
                                timer: 3000
                            }).then(() => {
                                location.reload(true);
                            })
                        }
                    })
                }
            })
        })
    </script>
@endsection
