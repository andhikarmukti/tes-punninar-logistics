@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <h1 class="text-center mb-4">Halaman Edit Gn Md</h1>
            <div class="col col-md-6">
                <a href="/master-lookup/gn-md" class="btn btn-primary mb-3">Back</a>
                <div class="card shadow-sm p-4">
                    @if(session()->has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('success') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                    <form action="/master-lookup/gn-md/{{ $gn_md->lookup_lines_id }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="mb-3">
                            <label for="lookup_id" class="form-label">Gn Mh Lookup</label>
                            <select name="lookup_id" id="lookup_id" class="form-select @error('lookup_id') is-invalid @enderror">
                                <option value="" selected disabled>Select Gn Mh Lookup</option>
                                @foreach ($gn_mhs as $gn_mh)
                                    <option {{ $gn_md->lookup_id == $gn_mh->lookup_id ? 'selected' : '' }} value="{{ $gn_mh->lookup_id }}">{{ $gn_mh->lookup_code }}</option>
                                @endforeach
                            </select>
                            @error('lookup_id')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="lookup_lines_code" class="form-label">Lookup Lines Code</label>
                            <input value="{{ old('lookup_lines_code') ?? $gn_md->lookup_lines_code }}" type="text" class="form-control @error('lookup_lines_code') is-invalid @enderror" id="lookup_lines_code" name="lookup_lines_code">
                            @error('lookup_lines_code')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="description" class="form-label">Description</label>
                            <input value="{{ old('description') ?? $gn_md->description }}" type="text" class="form-control @error('description') is-invalid @enderror" id="description" name="description">
                            @error('description')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="effective_from" class="form-label">Effective From</label>
                            <input value="{{ old('effective_from') ?? $gn_md->effective_from }}" type="date" class="form-control @error('effective_from') is-invalid @enderror" id="effective_from" name="effective_from">
                            @error('effective_from')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="effective_to" class="form-label">Effective To</label>
                            <input value="{{ old('effective_to') ?? $gn_md->effective_to }}" type="date" class="form-control @error('effective_to') is-invalid @enderror" id="effective_to" name="effective_to">
                            @error('effective_to')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary d-flex ms-auto">Edit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
