<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\GnMdLookupController;
use App\Http\Controllers\GnMhLookupController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\PowerUnitController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/power-unit');
});

Route::group(['middleware' => 'guest'], function()
{
    Route::get('/login', [AuthController::class, 'loginIndex'])->name('login');
    Route::post('/login', [AuthController::class, 'login']);
});

Route::group(['middleware' => 'auth'], function()
{
    Route::post('/logout', [AuthController::class, 'logout']);

    // Power Unit
    Route::get('/power-unit', [PowerUnitController::class, 'index'])->name('power-unit.index');
    Route::get('/power-unit/{id}', [PowerUnitController::class, 'edit']);
    Route::post('/power-unit', [PowerUnitController::class, 'store'])->name('power-unit.store');
    Route::put('/power-unit', [PowerUnitController::class, 'update'])->name('power-unit.update');
    Route::delete('/power-unit/{id}', [PowerUnitController::class, 'destroy']);

    // Location
    Route::get('/location', [LocationController::class, 'index']);
    Route::get('/location/{id}', [LocationController::class, 'show']);

    // Soal nomor 5
    Route::get('/soal-nomor-5', [PowerUnitController::class, 'soalNomor5']);

    // Master Lookup
    Route::group(['prefix'=>'master-lookup'], function(){
        // Gn Mh
        Route::get('/gn-mh', [GnMhLookupController::class, 'index']);
        Route::get('/gn-mh/{id}', [GnMhLookupController::class, 'edit']);
        Route::put('/gn-mh/{id}', [GnMhLookupController::class, 'update']);
        Route::delete('/gn-mh/{id}', [GnMhLookupController::class, 'destroy']);
        Route::post('/gn-mh', [GnMhLookupController::class, 'store']);

        // Gn md
        Route::get('/gn-md', [GnMdLookupController::class, 'index']);
        Route::get('/gn-md/{id}', [GnMdLookupController::class, 'edit']);
        Route::put('/gn-md/{id}', [GnMdLookupController::class, 'update']);
        Route::delete('/gn-md/{id}', [GnMdLookupController::class, 'destroy']);
        Route::post('/gn-md', [GnMdLookupController::class, 'store']);
    });

    Route::get('/soal-nomor-7-a', [PagesController::class, 'soalNomor7a']);
    Route::get('/soal-nomor-7-b', [PagesController::class, 'soalNomor7b']);
    Route::get('/soal-nomor-7-c', [PagesController::class, 'soalNomor7c']);
    Route::get('/soal-nomor-7-d', [PagesController::class, 'soalNomor7d']);
});
