<?php

use App\Http\Controllers\Api\ApiAuthController;
use App\Models\PowerUnit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ApiPowerUnitController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [ApiAuthController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum']], function() {
    // Power Unit
    Route::post('/power-unit', [ApiPowerUnitController::class, 'store']);
    Route::get('/power-unit', [ApiPowerUnitController::class, 'index']);
    Route::put('/power-unit', [ApiPowerUnitController::class, 'update']);
    Route::delete('/power-unit/{id}', [ApiPowerUnitController::class, 'destroy']);
});
